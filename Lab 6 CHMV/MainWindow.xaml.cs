﻿using Lab_6_CHMV.Classes;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Lab_6_CHMV
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly Username _username = Username.GetUserInfo();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void RefreshUsers()
        {
            listBoxUsers.Items.Clear();

            List<string> users = Database.GetUsers();

            for (int i = 0; i < users.Count; i++)
            {
                listBoxUsers.Items.Add(users[i]);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(_username.Login != "Admin")
            {
                buttonDeleteUser.Visibility = Visibility.Collapsed;
            }

            RefreshUsers();
        }

        private void buttonDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            string user = (string)listBoxUsers.SelectedItem;

            if(_username.Login == user)
            {
                MessageBox.Show("Вы не можете удалить сами себя!", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(user != null)
            {
                Database.DeleteUser(user);

                RefreshUsers();
            }

            else
            {
                MessageBox.Show("Выберите пользователя!", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
