﻿using Lab_6_CHMV.Classes;
using System;
using System.Windows;

namespace Lab_6_CHMV
{
    /// <summary>
    /// Логика взаимодействия для AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {
        private static readonly Username _username = Username.GetUserInfo();

        public AuthWindow()
        {
            InitializeComponent();
        }

        private void buttonAuth_Click(object sender, RoutedEventArgs e)
        {
            string login = textBoxLogin.Text;

            string password = textBoxPass.Text;

            if (Database.Auth(login, password))
            {
                _username.Login = login;

                _username.Password = password;

                Visibility = Visibility.Collapsed;

                MainWindow window = new MainWindow();

                window.ShowDialog();
            }

            else
            {
                MessageBox.Show("Неправильный логин и/или пароль!", "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
