﻿namespace Lab_6_CHMV.Classes
{
    public class Username
    {
        private static readonly Username _username = new Username();

        public string Login { get; set; }

        public string Password { get; set; }

        public static Username GetUserInfo()
        {
            return _username;
        }
    }
}
