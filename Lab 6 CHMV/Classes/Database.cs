﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;

namespace Lab_6_CHMV.Classes
{
    public static class Database
    {
        private static void CheckExistDatabase()
        {
            if(!File.Exists(FileNames.DatabaseName))
            {
                SQLiteConnection.CreateFile(FileNames.DatabaseName);
            }
        }

        public static bool Auth(string login, string password)
        {
            SQLiteConnection connection =
            new SQLiteConnection(string.Format("Data Source={0};", FileNames.DatabaseName));

            connection.Open();

            SQLiteCommand command = new SQLiteCommand(connection);

            command.CommandText = "SELECT * FROM users WHERE login = '" + login + "' AND password = '" + password + "'";

            var countRows = command.ExecuteScalar();

            if (countRows != null)
            {
                return true;
            }

            connection.Close();

            return false;
        }

        public static List<string> GetUsers()
        {
            SQLiteConnection connection =
            new SQLiteConnection(string.Format("Data Source={0};", FileNames.DatabaseName));

            connection.Open();

            SQLiteCommand command = new SQLiteCommand(connection);

            command.CommandText = "SELECT * FROM 'users'";

            SQLiteDataReader reader = command.ExecuteReader();

            List<string> users = new List<string>();

            foreach (DbDataRecord record in reader)
            {
                string userLogin = record["login"].ToString();
                users.Add(userLogin);
            }

            return users;
        }

        public static void DeleteUser(string user)
        {
            SQLiteConnection connection =
            new SQLiteConnection(string.Format("Data Source={0};", FileNames.DatabaseName));

            connection.Open();

            SQLiteCommand command = new SQLiteCommand(connection);

            command.CommandText = "DELETE FROM 'users' WHERE login = '" + user + "'";

            command.ExecuteNonQuery();

            connection.Close();
        }
    }
}
